# Some additional shell settings for inside the Docker container

export USER=`whoami`

# Appearance of command prompt
C_MB="\[\033[1;35m\]"
C_MD="\[\033[0;35m\]"
C_NM="\[\033[0m\]"
PS1="$C_MD\d\010\010\010\010\010\010\010 \t\010\010\010 $C_MB""$USER$C_MD at $C_MB\w$C_NM\n$C_MB\h$C_MD [$C_MB$?$C_MD]$C_MB\$$C_NM "

# Some aliases for faster bash usage
alias findsub="find . -type f -print | xargs grep"
alias findfile="find . -type f -print | grep"
alias filefind=findfile
alias dirfind="find . -type d -print | grep"

alias ls="/bin/ls -F --color=tty"
alias ll="ls -l"
alias la="ll -a"

# Fix color output of 'ls'
eval `dircolors --bourne-shell ~/.dir_colors`
