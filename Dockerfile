FROM   nrchkb/node-red-homekit:latest

ENV    TERM=xterm

COPY   .bashrc .dir_colors .vimrc /root/
COPY   etc/services.d/nodered/run /etc/services.d/nodered
RUN    chmod 644 /root/.bashrc /root/.dir_colors /root/.vimrc && \
       chmod 755 /etc/services.d/nodered/run && \
       \
       addgroup node-red users && \
       addgroup avahi users && \
       addgroup messagebus users && \
       \
       apk add --no-cache coreutils vim && \
       rm /usr/bin/vi && ln -s /usr/bin/vim /usr/bin/vi && \
       \
       npm install --save-exact --unsafe-perm --no-update-notifier --only=production \
           node-red-admin                \
           node-red-contrib-ccu          \
           node-red-contrib-influxdb     \
           node-red-contrib-sun-position \
           node-red-dashboard            \
           node-red-node-sqlite

VOLUME /data

# The port(s) of the included Homebridge(s) (make sure to set this value in the bridge config in Node-RED!)
EXPOSE 2058 2059

# The ports of the BinRPC and XmlRpc services started for each CCU connection
# Two ports are needed per connected CCU.
# The ports must match the configurations of your CCU connections in their Node-RED config nodes.
EXPOSE 2060 2061
EXPOSE 2062 2063
EXPOSE 2064 2065
EXPOSE 2066 2067
